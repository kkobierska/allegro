class Auction < ActiveRecord::Base
  belongs_to :user
  belongs_to :categories
  has_many :bids
  has_many :photos

  validates :initial_price, presence: true

  def end
    self.ended_at = Time.now
    if self.bids.any?
      self.winner_id = self.bids.last.user_id
    end
    self.save
  end
end
