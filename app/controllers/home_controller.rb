class HomeController < ApplicationController
  def index
    @user = current_user.email
    @auctions = Auction.all
  end
end