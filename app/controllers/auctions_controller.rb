class AuctionsController < ApplicationController
  before_filter :user_signed_in?

  def index
    @auctions = Auction.all
  end

  def my
    @auctions = current_user.auctions
  end

  def end
    @auction = Auction.find(params[:id])
    @auction.end
    redirect_to my_auctions_path
  end

  def new
    @auction = Auction.new
  end

  def create
    @auction = Auction.new(auction_params)
    @auction.user_id = current_user.id
    if @auction.save
      redirect_to auction_path(@auction)
    else
      render :new
    end
  end

  def update
    @auction = Auction.find(params[:id])
    if @auction.update(auction_params)
      redirect_to my_auctions_path
    end
  end

  def show
    @auction = Auction.find(params[:id])
    @bid = @auction.bids.build
    @current_price = @auction.initial_price
    @bids = @auction.bids.all
  end


  private

  def auction_params
    params.require(:auction).permit(:name, :description, :initial_price, :starts_at, :end_time, :user_id)
  end

end
