class BidsController < ApplicationController
  
  # def new
  #   @auction = Auction.find(params[:id])
  #   @bid = @auction.bids.build
  # end

  def create
    @auction = Auction.find(params[:auction_id])
    @bid = @auction.bids.new(bids_params)
    @bid.user_id = current_user.id
    @bid.current_price = @auction.initial_price + 1
    if @bid.save
      redirect_to auction_path(@auction)
    else
      # render 'auctions#show'
    end
  end

  private
  def bids_params
    params[:bid].permit(:max_price, :user_id, :auction_id)
  end
end
