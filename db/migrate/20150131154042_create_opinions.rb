class CreateOpinions < ActiveRecord::Migration
  def change
    create_table :opinions do |t|
      t.integer :user_id, null: false
      t.integer :auction_number
      t.text :description, null: false

      t.timestamps null: false
    end
  end
end
