class CreateAuctions < ActiveRecord::Migration
  def change
    create_table :auctions do |t|
      t.string :name, null: false
      t.text :description
      t.decimal :initial_price, null: false
      t.integer :user_id
      t.integer :winner_id
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :starts_at
      t.datetime :ended_at
      t.integer :length
      t.datetime :end_time

      t.timestamps null: false
    end
  end

end
