class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :auction_id, null: false

      t.timestamps null: false
    end
  end
end
