class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|
      t.integer :auction_id, null: false
      t.integer :user_id, null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.decimal :max_price, null: false
      t.decimal :current_price, null: false

      t.timestamps null: false
    end
  end
end
